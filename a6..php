<html>
  <head>
    <meta charset="utf-8">
    <title>Sellama.lk</title>
    <link href="images/favicon-16x16.png" type="image/png" rel="icon" sizes="16x16">
    <link href="images/favicon-32x32.png" type="image/png" rel="icon" sizes="32x32">
    <link rel="stylesheet" href="style.css">
    <script src="sdig6.js"></script>
  </head>
  <body class="EN">

<?php
include 'inc/header.php';

Session::CheckSession();

$logMsg = Session::get('logMsg');
if (isset($logMsg)) {
  echo $logMsg;
}
$msg = Session::get('msg');
if (isset($msg)) {
  echo $msg;
}
Session::set("msg", NULL);
Session::set("logMsg", NULL);
?>
<?php

if (isset($_GET['remove'])) {
  $remove = preg_replace('/[^a-zA-Z0-9-]/', '', (int)$_GET['remove']);
  $removeUser = $users->deleteUserById($remove);
}

if (isset($removeUser)) {
  echo $removeUser;
}
if (isset($_GET['deactive'])) {
  $deactive = preg_replace('/[^a-zA-Z0-9-]/', '', (int)$_GET['deactive']);
  $deactiveId = $users->userDeactiveByAdmin($deactive);
}

if (isset($deactiveId)) {
  echo $deactiveId;
}
if (isset($_GET['active'])) {
  $active = preg_replace('/[^a-zA-Z0-9-]/', '', (int)$_GET['active']);
  $activeId = $users->userActiveByAdmin($active);
}

if (isset($activeId)) {
  echo $activeId;
}


 ?>
      <div class="card ">
        <div class="card-header">
          <h3><i class="fas fa-users mr-2"></i>User list <span class="float-right">Welcome! <strong>
            <span class="badge badge-lg badge-secondary text-white">
<?php
$username = Session::get('username');
if (isset($username)) {
  echo $username;
}
 ?></span>

          </strong></span></h3>
        </div>
       <div class="menu">
      <span class="" title="R_100">100</span>
      <span class="menu-active" title="R_10">10</span>
      <span class="" title="R_25">25</span>
      <span class="" title="R_50">50</span>
      <span class="" title="R_75">75</span>
      <span class="" title="RDBEAR">BEAR</span>
      <span class="" title="RDBULL">BULL</span>
    </div>
   <div class="chartContainer" id="chartContainer"></div>
   <div id="SpotArrow" class="SpotArrow">
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
 <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
 <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      </div>
  
    <div id="headcol" class="headcol">
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
 <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
 <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span>
      <span class="_"></span> 
    </div>
   


    <div id="arrow_up" class="arrow_up">
      <span class="_"></span>
      </div>
     <div id="arrow_down" class="arrow_down">
      <span class="_"></span>
     </div>
 
    <script src="canvasjs.min.js"></script>
      </div>



  <?php
  include 'inc/footer.php';

  ?>
 </body>
</html>
